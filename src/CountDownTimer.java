import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by julius on 17/07/17.
 */
public class CountDownTimer implements ActionListener {
    Timer t = new Timer(1, this);
    int milliseconds = 0;
    int seconds = 0;
    int minutes = 5;
    public void startCountDown(){
        t.start();
    }
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(milliseconds == 0){
            if(seconds == 0){
                if(minutes == 0){
                    Game.g.gameOver = true;
                }else{
                    minutes--;
                    seconds = 59;
                    milliseconds = 999;
                    return;
                }
            }else{
                seconds--;
                milliseconds = 999;
                return;
            }
        }else {
            milliseconds--;
        }
    }
    public void reset(){
        milliseconds = 0;
        seconds = 0;
        minutes = 5;
    }
}
