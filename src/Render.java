import javax.swing.*;
import java.awt.*;

/**
 * Created by julius on 16/07/17.
 */
public class Render extends JPanel{
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;
        Game.g.repaint(g2D);
    }
}
