import javax.imageio.ImageIO;
import java.applet.Applet;
import java.awt.*;
import java.io.File;

/**
 * Created by julius on 17/07/17.
 */
public class ImageLoader extends Applet{
    private Image img;
    public Image loadImage(String path) {
        try{
            img = ImageIO.read(new File(path));
        }catch(Exception e){}
        return img;
    }
}
