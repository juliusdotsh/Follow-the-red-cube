import java.awt.*;
import java.awt.event.*;

/**
 * Created by julius on 16/07/17.
 */
public class KeyInput extends KeyAdapter{
    int VelX,VelY;
    Player player = Game.player;
    public void keyPressed(KeyEvent ke) {
        if(!Game.g.gameOver) {
            if (ke.getKeyCode() == KeyEvent.VK_W) {
                System.out.println(player.x + " " + player.y);
                VelY = -5;
            }
            if (ke.getKeyCode() == KeyEvent.VK_A) {
                System.out.println(player.x + " " + player.y);
                VelX = -5;
            }
            if (ke.getKeyCode() == KeyEvent.VK_S) {
                System.out.println(player.x + " " + player.y);
                VelY = 5;
            }
            if (ke.getKeyCode() == KeyEvent.VK_D) {
                System.out.println(player.x + " " + player.y);
                VelX = 5;
            }
        }else{
            if(ke.getKeyCode() == KeyEvent.VK_ENTER){
                Game.g.ct.reset();
                Game.g.gameOver = false;
                Game.g.scoreN = 0;
            }
        }
    }
    public void keyReleased(KeyEvent ke) {
        if(!Game.g.gameOver) {
            if (ke.getKeyCode() == KeyEvent.VK_W) {
                VelY = 0;
            }
            if (ke.getKeyCode() == KeyEvent.VK_A) {
                VelX = 0;
            }
            if (ke.getKeyCode() == KeyEvent.VK_S) {
                VelY = 0;
            }
            if (ke.getKeyCode() == KeyEvent.VK_D) {
                VelX = 0;
            }
        }else{}
    }
}
