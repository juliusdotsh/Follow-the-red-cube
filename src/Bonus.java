import java.awt.*;

/**
 * Created by julius on 17/07/17.
 */
public class Bonus extends Score{
    boolean onScreen = false;
    boolean createBonus = false;
    Image image;
    private String path = "images/cloak.png";
    public Bonus(){
        ImageLoader imageLoader = new ImageLoader();
        image = imageLoader.loadImage(path);
        size = image.getHeight(null);
    }
    public void spawnBonus(){
        if(!onScreen){
            createBonus = true;
        }
    }
}
