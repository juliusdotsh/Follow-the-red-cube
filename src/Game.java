import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by julius on 16/07/17.
 */
public class Game  implements ActionListener {
    static Game g;
    private static final int SIZE = 800, scaleToFixHeightBug = 38;
    Render render;
    static Player player;
    KeyInput keyInput;
    Score score;
    int xBound, yBound;
    JFrame frame;
    int scoreN = 0;
    Bonus bonus = new Bonus();
    boolean gameOver = false;
    CountDownTimer ct;
    public Game(){
        render = new Render();
        player = new Player();
        keyInput = new KeyInput();
        score = new Score();
        Timer timer = new Timer(20,this);
        frame = new JFrame("Follow the red cube");
        frame.add(render);
        frame.setSize(SIZE,SIZE+scaleToFixHeightBug);
        frame.addKeyListener(keyInput);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        xBound = SIZE;
        yBound = xBound;
        ct = new CountDownTimer();
        ct.startCountDown();
        timer.start();
    }
    public void repaint(Graphics2D g) {
        if(!gameOver){
            g.setColor(Color.BLACK);
            g.setFont(new Font("Arial", Font.BOLD, 20));
            g.drawString(scoreN + "", SIZE / 2, 20);
            g.drawString(ct.minutes + " : " + ct.seconds, SIZE - g.getFontMetrics().stringWidth(ct.minutes + " : " + ct.seconds), 20);
            
            g.fillRect(player.x, player.y, player.size, player.size);

            g.setColor(Color.RED);
            g.fillRect(score.x, score.y, score.size, score.size);

            if (bonus.createBonus) {
                g.drawImage(bonus.image, bonus.x, bonus.y, null);
                bonus.onScreen = true;
            }
        }else{
            gameOver(g);
        }
    }

    private void gameOver(Graphics2D g) {
        g.setColor(Color.BLACK);
        g.setFont(new Font("Arial", Font.BOLD, 20));

        g.drawString( "You have scored :"+scoreN, SIZE / 2 - g.getFontMetrics().stringWidth("You have scored :"+scoreN)/2, (SIZE/2)-50);
        g.drawString( "GAME OVER", SIZE / 2 - g.getFontMetrics().stringWidth("GAME OVER")/2, (SIZE/2)+20);
        g.drawString( "Press enter to continue", SIZE / 2 - g.getFontMetrics().stringWidth("Press enter to continue")/2, (SIZE/2)+50);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        onUpdate();
        render.repaint();
    }
    public void onUpdate() {
        //player position updater
        player.x += keyInput.VelX;
        player.y += keyInput.VelY;

        if (player.x < 0) {
            player.x = 0;
        } else if (player.x + player.size > xBound) {
            player.x = SIZE - player.size;
        }
        if (player.y < 0) {
            player.y = 0;
        } else if (player.y + player.size > yBound) {
            player.y = SIZE - player.size;
        }

        checkScore();
        if(bonus.onScreen){
            checkBonus();
        }else if(bonus.random.nextInt(10000) == 1){
            bonus.spawnBonus();
        }
    }
    public void checkScore(){
        Rectangle rect1 = new Rectangle(player.x, player.y, player.size, player.size);
        Rectangle rect2 = new Rectangle(score.x, score.y, score.size, score.size);
        if (rect1.getBounds().intersects(rect2.getBounds())) {
            score.rearrange();
            scoreN++;
        }
    }
    public void checkBonus(){
        Rectangle rect1 = new Rectangle(player.x, player.y,player.size,player.size);
        Rectangle rect2 = new Rectangle(bonus.x,bonus.y,bonus.size,bonus.size);
        if(rect1.getBounds().intersects(rect2.getBounds())) {
            bonus.rearrange();
            bonus.onScreen = false;
            bonus.createBonus = false;
            if (ct.seconds + 30 > 60) {
                ct.seconds -= 60;
                ct.minutes += 1;
                ct.seconds += 30;
            } else {
                ct.seconds += 30;
            }
        }
    }
    public static void main(String[] args) {g = new Game();
    }
}
